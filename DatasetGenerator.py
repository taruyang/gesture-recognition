#!/usr/bin/env python
# Jongho Yang, taruyang@gmail.com
import numpy as np
import os
from os import listdir
from os.path import isfile, join
import cv2
import FeatureGenerator as fg
from random import shuffle

module_name = "DatasetGenerator"
default_dir_path = './datasets/'
default_data_path = './features.data'

class DatasetGenerator:
    def __init__(self):
        self.dir_path = ''
        self.feature_gen = fg.FeatureGenerator()

    def getClassNum(self, file_name):
        class_num = -1
        pos = file_name.find('_')
        class_num = int(file_name[pos + 1])
        return class_num

    def SetSource(self, dirPath):
        self.dir_path = dirPath

    def GenerateDataset(self, file_path):
        file_names = [f for f in listdir(self.dir_path) if isfile(join(self.dir_path, f))]
        shuffle(file_names)

        fd = open(file_path, 'w')
        for i in range(len(file_names)):
            # get classnum
            class_num = self.getClassNum(file_names[i])
            print("[%s] Start to get features from %s (class: %d) " % (module_name, file_names[i], class_num))

            # get Features
            self.feature_gen.SetFileSource(self.dir_path + file_names[i])
            descriptors = self.feature_gen.GetFourierDescriptors(False)
            # write class and features
            fd.write('%d,' % (class_num))
            descriptors.tofile(fd, sep=',', format='%.7f')
            fd.write('\n')

        fd.close()

if __name__ == '__main__':
    dataset_gen = DatasetGenerator()
    dataset_gen.SetSource(default_dir_path)
    dataset_gen.GenerateDataset(default_data_path)

    cv2.destroyAllWindows()


# if i == 0:
#    self.features = self.feature_gen.GetFourierDescriptors(False)
# else:
#    descriptors = self.feature_gen.GetFourierDescriptors(False)
#    self.features = np.concatenate((self.features, descriptors))
