#!/usr/bin/env python

'''
Gesture Recognition (Jongho Yang, taruyang@gmail.com)
USAGE:
    GestureRecognition.py [--classifier <name>][--load <path for trained network>][--file <file name to be recognized>]

Available classifier: SVM, RTrees, MLP
Default Values:
    --classifier RTrees
    --load './gesture.xml'
'''
import numpy as np
import cv2
import os
import FeatureGenerator as fg
import ClassifierBuilder as cb

module_name = "Classifier"
default_clissifier = 'RTrees'
default_load_path = './gesture.xml'

class GestureRecognition():
    def __init__(self):
        self.classifiers = [cb.SVM, cb.RTrees, cb.MLP]
        self.classifiers = dict([(classifier.__name__.lower(), classifier) for classifier in self.classifiers])
        self.feature_gen = fg.FeatureGenerator()
        self.SRC_WINDOW = "FeatureSource"
        self.isImage = False

    def LoadClassifier(self, classifier_name, load_path):
        # Create Classifier
        cf_class = self.classifiers[classifier_name.lower()]
        self.classifier = cf_class(load_path)

    def PridictFile(self, file_path):
        src_color = cv2.imread(file_path, cv2.IMREAD_COLOR)
        self.isImage = True
        self.PredictBuffer(src_color)

    def PredictBuffer(self, srcColor):
        self.feature_gen.SetBufferSource(srcColor)
        descriptors = self.feature_gen.GetFourierDescriptors(True)
        if len(descriptors):
            r = self.classifier.predict(descriptors)
            print("[%s] Predicted value: %f" % (module_name, r))

            printit = "Gesture: " + str(int(r))
            cv2.putText(srcColor, printit, (10,30), cv2.FONT_HERSHEY_PLAIN, 1.5, (0,255,0), 2, 8)

        cv2.imshow( self.SRC_WINDOW, srcColor)

        if self.isImage:
            cv2.waitKey(0)

if __name__ == '__main__':
    import getopt
    import sys

    print(__doc__)

    optlist, dummy = getopt.getopt(sys.argv[1:], '', ['classifier=', 'load=', 'file='])
    optlist = dict(optlist)

    # Set Default values
    optlist.setdefault('--classifier', default_clissifier)
    optlist.setdefault('--load', default_load_path)
    #optlist.setdefault('--file', 'hand5_9_bot_seg_5_cropped.png')

    # Read options
    classifier_name = optlist['--classifier']
    load_path = optlist['--load']
    video_mode = False;
    try:
        file_path = optlist['--file']
    except:
        video_mode = True

    print('[%s] Opts Classifier:%s load:%s video_mode:%r' % (module_name, classifier_name, load_path, video_mode))

    gesture_recognizer = GestureRecognition()
    gesture_recognizer.LoadClassifier(classifier_name, load_path)

    if video_mode == False:
        gesture_recognizer.PridictFile(file_path)
    else:
        cap = cv2.VideoCapture(0)

        while(True):
            ret, frame = cap.read()
            if ret == False:
                break

            key = cv2.waitKey(10)
            if key==113 or key==27:
                break

            gesture_recognizer.PredictBuffer(frame)

    cv2.destroyAllWindows()