#!/usr/bin/env python
# Jongho Yang, taruyang@gmail.com

import numpy as np
import cv2
import os

class FeatureGenerator():
    NAME = "FeatureGenerator"
    PROC_WINDOW = "SegmentedSource"
    OUT_WINDOW = "ContourImage"
    NUM_CE = 20

    def __init__(self):
        cv2.namedWindow( self.PROC_WINDOW, cv2.WINDOW_AUTOSIZE )
        cv2.namedWindow( self.OUT_WINDOW, cv2.WINDOW_AUTOSIZE )
        _largestContour = 0;

    def SetFileSource(self, path):
        if not os.path.isfile(path):
            print('[%s] setSource Error: %s is not valid input' % (self.NAME, path))
            return

        self.srcColor = cv2.imread(path, cv2.IMREAD_COLOR)

    def SetBufferSource(self, src):
        self.srcColor = src.copy()

    def extractContour(self, showProcess):
        srcGray = cv2.cvtColor(self.srcColor, cv2.COLOR_BGR2GRAY)
        srcGray = cv2.blur(srcGray, (3,3))
        ret, outSegmented = cv2.threshold(srcGray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

        # Find contours
        image, self.contours, hierarchy = cv2.findContours(outSegmented, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        # Draw outer contours
        width, height = outSegmented.shape
        outContour = np.zeros((width, height, 3), np.uint8)

        largeSize = 0
        self.largestContour = -1
        for i in range(len(self.contours)):
            if largeSize < self.contours[i].size:
                largeSize = self.contours[i].size
                self.largestContour = i

        outContour = cv2.drawContours( outContour, self.contours, self.largestContour, ( 0, 255, 0 ), 1, 8);

        if showProcess:
            # Show result
            cv2.imshow( self.PROC_WINDOW, outSegmented )
            cv2.imshow( self.OUT_WINDOW, outContour)

    def GetFourierDescriptors(self, showProcess):
        self.extractContour(showProcess)
        if self.largestContour == -1:
            return []

        contour = self.contours[self.largestContour]

        numContourPoints, item_n, item_size,  = contour.shape

        ax = []; ay = []; bx = []; by = []
        for k in range(self.NUM_CE):
            ax.append(0.0); ay.append(0.0)
            bx.append(0.0); by.append(0.0)
            theta = 0.0

            for u in range(numContourPoints):
                theta = (2 * np.pi * u * (k + 1)) / numContourPoints
                x = contour[u][0][0]; y = contour[u][0][1]
                ax[k] = ax[k] + x * np.cos(theta)
                bx[k] = bx[k] + x * np.sin(theta)
                ay[k] = ay[k] + y * np.cos(theta)
                by[k] = by[k] + y * np.sin(theta)

            ax[k] = ax[k] / numContourPoints; ay[k] = ay[k] / numContourPoints
            bx[k] = bx[k] / numContourPoints; by[k] = by[k] / numContourPoints

        descriptors = []
        for k in range(self.NUM_CE):
            value = np.sqrt((ax[k]*ax[k] + ay[k]*ay[k]) / (ax[0]*ax[0] + ay[0]*ay[0]))  + \
                             np.sqrt((bx[k]*bx[k] + by[k]*by[k]) / (bx[0]*bx[0] + by[0]*by[0]))
            descriptors.append(value)

        features = []
        features.append(descriptors)
        return np.asarray(features, dtype=np.float32)

if __name__ == '__main__':
    featureGenerator = FeatureGenerator()
    featureGenerator.SetFileSource('hand1_0_bot_seg_1_cropped.png')
    descripters = featureGenerator.GetFourierDescriptors(True)

    cv2.waitKey(0)
    cv2.destroyAllWindows()