#!/usr/bin/env python

'''
Classifier Builder (Jongho Yang, taruyang@gmail.com)
USAGE:
    ClassifierBuilder.py [--classifier <name>][--data <data file path>][--ratio <train ratio>][--save <path for trained network>]

Available classifier: SVM, RTrees, KNearest, MLP
Default Values:
    --classifier RTrees
    --data ./features.data
    --save './gesture.xml'
    --ratio '0.8'
'''
import numpy as np
import cv2

module_name = "Builder"
default_clissifier = 'RTrees'
default_data_path = './features.data'
default_save_path = './gesture.xml'
default_train_ratio = '0.8'
class_count = 10

class SVM():
    def __init__(self, fn = 'None'):
        if fn == 'None':
            self.classifier = cv2.ml.SVM_create()
        else:
            self.classifier = cv2.ml.SVM_load(fn)

    def train(self, samples, responses):
        self.classifier.setType(cv2.ml.SVM_C_SVC)
        self.classifier.setC(5)
        self.classifier.setKernel(cv2.ml.SVM_RBF)
        self.classifier.setGamma(5)
        self.classifier.train(samples, cv2.ml.ROW_SAMPLE, responses.astype(int))

    def predict(self, features):
        _ret, resp = self.classifier.predict(features)
        return resp.ravel()

    def save(self, fn):
        self.classifier.save(fn)

class RTrees():
    def __init__(self, fn = 'None'):
        if fn == 'None':
            self.classifier = cv2.ml.RTrees_create()
        else:
            self.classifier = cv2.ml.RTrees_load(fn)

    def train(self, features, labels):
        self.classifier.setMaxDepth(20)
        self.classifier.train(features, cv2.ml.ROW_SAMPLE, labels.astype(int))

    def predict(self, features):
        _ret, resp = self.classifier.predict(features)
        return resp.ravel()

    def save(self, fn):
        self.classifier.save(fn)

class KNearest():
    def __init__(self, fn = 'None'):
        self.classifier = cv2.ml.KNearest_create()

    def train(self, features, labels):
        self.classifier.train(features, cv2.ml.ROW_SAMPLE, labels)

    def predict(self, features):
        _retval, results, _neigh_resp, _dists = self.classifier.findNearest(features, k = 3)
        return results.ravel()

    def save(self, fn):
        self.classifier.save(fn)

class MLP():
    def __init__(self, fn = 'None'):
        if fn == 'None':
            self.classifier = cv2.ml.ANN_MLP_create()
        else:
            self.classifier = cv2.ml.ANN_MLP_load(fn)

    def unroll(self, labels):
        label_n = len(labels)
        new_labels = np.zeros(label_n * class_count, np.int32)
        cls_label = np.int32( labels + np.arange(label_n) * class_count )
        new_labels[cls_label] = 1
        return new_labels

    def train(self, features, labels):
        _feature_n, var_n = features.shape
        new_labels = self.unroll(labels).reshape(-1, class_count)
        layer_sizes = np.int32([var_n, 100, 100, class_count])

        self.classifier.setTrainMethod(cv2.ml.ANN_MLP_BACKPROP)
        self.classifier.setLayerSizes(layer_sizes)
        self.classifier.setActivationFunction(cv2.ml.ANN_MLP_SIGMOID_SYM, 0, 0)
        self.classifier.setBackpropWeightScale(0.001)
        self.classifier.setTermCriteria((cv2.TERM_CRITERIA_COUNT, 1500, 0.001))
        self.classifier.train(features, cv2.ml.ROW_SAMPLE, np.float32(new_labels))

    def predict(self, samples):
        _ret, resp = self.classifier.predict(samples)
        return resp.argmax(-1)

    def save(self, fn):
        self.classifier.save(fn)

# Load data from the given text file.
def load_dataset(fname):
    dataset = np.loadtxt(fname, np.float32, delimiter=',', converters={ 0 : lambda ch : ord(ch)-ord('0') })
    features, labels = dataset[:,1:], dataset[:,0]
    return features, labels

if __name__ == '__main__':
    import getopt
    import sys

    print(__doc__)

    classifiers = [SVM, RTrees, KNearest, MLP]
    classifiers = dict( [(classifier.__name__.lower(), classifier) for classifier in classifiers] )

    optlist, dummy = getopt.getopt(sys.argv[1:], '', ['classifier=', 'data=', 'ratio=', 'save='])
    optlist = dict(optlist)

    # Set Default values
    optlist.setdefault('--classifier', default_clissifier)
    optlist.setdefault('--data', default_data_path)
    optlist.setdefault('--save', default_save_path)
    optlist.setdefault('--ratio', default_train_ratio)

    # Read options
    data_path = optlist['--data']
    classifier_name = optlist['--classifier']
    save_path = optlist['--save']
    train_ratio = float(optlist['--ratio'])
    print('[%s] Opts Classifier:%s data:%s ratio:%f save:%s ' % (module_name, classifier_name, data_path, train_ratio, save_path))

    # Load Dataset
    print('[%s] Load data from %s ' % (module_name, data_path))
    features, labels = load_dataset(data_path)
    Builder = classifiers[classifier_name.lower()]
    builder = Builder()

    train_n = int(len(features) * train_ratio)

    print('[%s] Trains dataset by %s ' % (module_name, Builder.__name__))
    builder.train(features[:train_n], labels[:train_n])

    print('[%s] Tests dataset ' % module_name)
    train_score = np.mean(builder.predict(features[:train_n]) == labels[:train_n].astype(int))
    test_score  = np.mean(builder.predict(features[train_n:]) == labels[train_n:].astype(int))

    print('[%s] Classifier Scores: train data: %f  test data: %f' % (module_name, train_score*100, test_score*100))

    print('[%s] Save trained network to %s ' % (module_name, save_path))
    builder.save(save_path)

    cv2.destroyAllWindows()