# Human hand Gesture recognition with OpenCV  #

This README would normally document whatever steps are necessary to get your application up and running.

## Introduction ##

As the development of the science and technology, an increasing number of body recognition applications walk into people's daily life. In this sproject, we are using python language to develop a gesture recognition application. This app works by three steps , the first step is Deskewing or also called Preprocessing, it could understand as converting colorful pictures into pictures with black and white pixels. The second step is based on the pictures with black and white pixels, we will convert the grayscale image to a feature vector . The last step is Training a Model or Learning a Classifier,we have chosen Support Vector Machines (SVM),Random tree and Neural networks as our classification algorithm, we put training image data to do the machine learning. Our app can recognize the hand gesture from 0 to 9. We will use 700 images about number hand gestures as our training data, no matter how the gesture angle and size changes, the app can always recognize our gestures.Our program is composed of four parts including ClassifierBuilder, DatasetGenerator, FeatureGenerator and GestureRecognition

## 1.Problem ##

How to use machine learning to build a precise gesture recognition programs, which would not effect different movement, rotation and scale.

Sub questions:

1. How to set up the feature vectors.
2. What is the feature vectors.
3. How can we put the training data into the programs, what kind of gesture we will predict.
4. Which machine learning methods we will use, how algorithms can bring a high accurate recognition rate.

## 2.Feature sets ##
### What is OpenCV? ###
OpenCV (Open Source Computer Vision) is a library of programming functions mainly aimed at real-time computer vision. Originally developed by Intel, it was later supported by Willow Garage and is now maintained by Itseez. The library is cross-platform and free for use under the open-source BSD license.OpenCV supports the Deep Learning frameworks TensorFlow, Torch/PyTorch and Caffe.

It has C++, C, Python, Java and MATLAB interfaces and supports Windows, Linux, Android and Mac OS. OpenCV leans mostly towards real-time vision applications and takes advantage of MMX and SSE instructions when available. A full-featured CUDA and OpenCL interfaces are being actively developed right now. There are over 500 algorithms and about 10 times as many functions that compose or support those algorithms.

## 3. Feature Extraction ##
To extract datasets from total 700 hand gesture images, two executable python files are implemented.  
 
    ** FeatureGenerator.py for generating features**  
    ** DatasetGenerator.py for generating datasets**  

Through this section, the detail process of two programs will be explained.

### 1) Generating features (FeatureGenerator.py) ###
To represent the shape of a hand gesture, 20 Fourier Descriptors are extracted per an image. <br>
The input of this program could be a file path or a buffer containing the data of an image. <br>
Even though the number of Fourier Descriptors could be infinite, in this paper, <br>
20 Fourier Descriptors are utilized because the effect of following descriptors are too small.<br>
The values of over 20 descriptors represent very high frequencies in shape which are not closely related with the contour of a hand gesture.<br>
It is also confirmed to restore original contour with 20 Fourier descriptors.

To handle the described processes, a class called "FeatureGenerator" is implemented.
The class, internally, has 5 methods for each steps.
The defined class will be called during the datasets generation process <br>
and at the final gesture recognition program.
Also, this file can be executed for self verification without the other file.

*** How to execute for self verification *** <br> 
&nbsp;&nbsp;&nbsp;python ./FeatureGenerator.py

*** Major methods *** 
* def SetFileSource(self, path)
   def SetBufferSource(self, src)   
&nbsp;&nbsp;&nbsp;Read an image data from a file or a NumPy.ndarray 

* def extractContour(self, showProcess)   
&nbsp;&nbsp;&nbsp;Extract the contour shape of input image with OpenCV APIs

* def GetFourierDescriptors(self, showProcess)  
&nbsp;&nbsp;&nbsp;Extract 20 Fourier Descriptors from the extracted contour set and Return the extracted 20 FDs.<br>
&nbsp;&nbsp;&nbsp;If the showProcess is True, the method shows the output during processes.

*** Implemented program *** <br> 

```
import numpy as np
import cv2
import os

class FeatureGenerator():
    NAME = "FeatureGenerator"
    PROC_WINDOW = "SegmentedSource"
    OUT_WINDOW = "ContourImage"
    NUM_CE = 20

    def __init__(self):
        cv2.namedWindow( self.PROC_WINDOW, cv2.WINDOW_AUTOSIZE )
        cv2.namedWindow( self.OUT_WINDOW, cv2.WINDOW_AUTOSIZE )
        _largestContour = 0;

    def SetFileSource(self, path):
        if not os.path.isfile(path):
            print('[%s] setSource Error: %s is not valid input' % (self.NAME, path))
            return

        self.srcColor = cv2.imread(path, cv2.IMREAD_COLOR)

    def SetBufferSource(self, src):
        self.srcColor = src.copy()

    def extractContour(self, showProcess):
        srcGray = cv2.cvtColor(self.srcColor, cv2.COLOR_BGR2GRAY)
        srcGray = cv2.blur(srcGray, (3,3))
        ret, outSegmented = cv2.threshold(srcGray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

        # Find contours
        image, self.contours, hierarchy = cv2.findContours(outSegmented, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        # Draw outer contours
        width, height = outSegmented.shape
        outContour = np.zeros((width, height, 3), np.uint8)

        largeSize = 0
        self.largestContour = -1
        for i in range(len(self.contours)):
            if largeSize < self.contours[i].size:
                largeSize = self.contours[i].size
                self.largestContour = i

        outContour = cv2.drawContours( outContour, self.contours, self.largestContour, ( 0, 255, 0 ), 1, 8);

        if showProcess:
            # Show result
            cv2.imshow( self.PROC_WINDOW, outSegmented )
            cv2.imshow( self.OUT_WINDOW, outContour)

    def GetFourierDescriptors(self, showProcess):
        self.extractContour(showProcess)
        if self.largestContour == -1:
            return []

        contour = self.contours[self.largestContour]

        numContourPoints, item_n, item_size,  = contour.shape

        ax = []; ay = []; bx = []; by = []
        for k in range(self.NUM_CE):
            ax.append(0.0); ay.append(0.0)
            bx.append(0.0); by.append(0.0)
            theta = 0.0

            for u in range(numContourPoints):
                theta = (2 * np.pi * u * (k + 1)) / numContourPoints
                x = contour[u][0][0]; y = contour[u][0][1]
                ax[k] = ax[k] + x * np.cos(theta)
                bx[k] = bx[k] + x * np.sin(theta)
                ay[k] = ay[k] + y * np.cos(theta)
                by[k] = by[k] + y * np.sin(theta)

            ax[k] = ax[k] / numContourPoints; ay[k] = ay[k] / numContourPoints
            bx[k] = bx[k] / numContourPoints; by[k] = by[k] / numContourPoints

        descriptors = []
        for k in range(self.NUM_CE):
            value = np.sqrt((ax[k]*ax[k] + ay[k]*ay[k]) / (ax[0]*ax[0] + ay[0]*ay[0]))  + \
                             np.sqrt((bx[k]*bx[k] + by[k]*by[k]) / (bx[0]*bx[0] + by[0]*by[0]))
            descriptors.append(value)

        features = []
        features.append(descriptors)
        return np.asarray(features, dtype=np.float32)
```

The source code for self verification is like the below. <br>
For the purpose, one image file, hand1_0_bot_seg_1_cropped.png, is used in this code.

```
if __name__ == '__main__':
    featureGenerator = FeatureGenerator()
    featureGenerator.SetFileSource('hand1_0_bot_seg_1_cropped.png')
    descripters = featureGenerator.GetFourierDescriptors(True)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
```
![alt text]()